const button = document.querySelector('button');
const input = document.createElement('input');
input.setAttribute('type', 'number');
input.setAttribute('placeholder', 'Диаметр круга');

button.addEventListener('click', drawCircle);


function drawCircle() {
    /*create input and button*/
    const btnDraw = document.createElement('button');
    btnDraw.innerText = 'Нарисовать';

    document.body.appendChild(input);
    document.body.appendChild(btnDraw);

    /*create section for circles and draw circles*/
    function draw() {
        const box = document.createElement('section');
        const diameter = +input.value;

        document.body.appendChild(box);

        for (let i = 0; i < 100; i++) {
            const circle = document.createElement('div');

            circle.style.width = `${diameter}px`;
            circle.style.height = `${diameter}px`;
            circle.style.borderRadius = `50%`;
            circle.style.backgroundColor = getRandomColor();

            function getRandomColor() {
                const letters = '0123456789ABCDEF';
                let color = '#';
                for (let i = 0; i < 6; i++) {
                    color += letters[Math.floor(Math.random() * 16)];
                }
                return color;
            }

            box.appendChild(circle);
        }
        /*remove circle*/
        box.addEventListener('click', (event) => {
            event.target.remove();
        });
    }

    btnDraw.addEventListener('click', draw);
}



